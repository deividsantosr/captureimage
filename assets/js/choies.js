function Choies() {
    'use strict';

    this.constructor();
    this.init();
}

Choies.prototype.constructor = function () {
    this.object = {
        items: []
    };
};

Choies.prototype.init = function () {
    this.scanImage()
        .setStorage();
};

//Change this method on other occasions
Choies.prototype.scanImage = function () {
    var self = this;

    jQuery('.thumbnail-slide .list-item img').each(function () {
        var alt = jQuery(this).attr('alt');
        var src = jQuery(this).attr('bigimg');

        self.setObject(alt, src);
    });

    return this;
};

Choies.prototype.setStorage = function () {
    chrome.storage.sync.set(this.object, function () {
        console.log('Saved configurations...');
    });

    return this;
};

Choies.prototype.setObject = function (alt = 'Sem nome', src = '') {
    this.object.items.push([alt, src]);

    return this;
};