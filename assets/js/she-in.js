function SheIn() {
    'use strict';

    this.constructor();
    this.init();
}

SheIn.prototype.constructor = function () {
    this.object = {
        items: []
    };
};

SheIn.prototype.init = function () {
    this.scanImage()
        .setStorage();
};

//Change this method on other occasions
SheIn.prototype.scanImage = function () {
    var self = this;

    jQuery('.other_Imgs img[data-src]').each(function () {
        var alt = jQuery('.good_descright h1').text();
        var src = jQuery(this).attr('data-src');

        self.setObject(alt, src);
    });

    return this;
};

SheIn.prototype.setStorage = function () {
    chrome.storage.sync.set(this.object, function () {
        console.log('Saved configurations...');
    });

    return this;
};

SheIn.prototype.setObject = function (alt = 'Sem nome', src = '') {
    this.object.items.push([alt, src]);

    return this;
};