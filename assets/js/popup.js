function Popup() {
    'use strict';

    this.title = null;

    this.init();
}

Popup.prototype.init = function () {
    this.setTitle()
        .showList()
        .downloadAll()
        .copyright();
};

Popup.prototype.showList = function () {
    var self = this;

    chrome.storage.sync.get('items', function (obj) {
        var items = obj.items;
        var parent = jQuery('.content .list');

        if (items.length > 0) {
            for (var i = 0; i < items.length; i++) {
                var alt = items[i][0];
                var src = items[i][1];

                parent.append(self.getItem(alt, src));
            }
        }
        else {
            parent.append(self.getNoItem());
        }
    });

    return this;
};

Popup.prototype.downloadAll = function () {
    var allDownload = jQuery('header .download-all');

    allDownload.fadeOut('fast');

    setTimeout(function () {
        var downloads = document.querySelectorAll('.content a');

        allDownload.bind('click', function () {
            for (var i = 0; i < downloads.length; i++) {
                downloads[i].click();
            }
        });

        allDownload.fadeIn('fast');
    }, 100);

    return this;
};

Popup.prototype.copyright = function () {
    var content = jQuery('.content');
    var copyright = parent = jQuery('.copyright');
    var toCopyright = jQuery('img.to-copyright');

    toCopyright.bind('click', function () {
        content.addClass('close');
    });

    copyright.bind('click', function () {
        content.removeClass('close');
    });

    return this;
};

Popup.prototype.getItem = function (alt, src) {
    html = '<div class="item">';
    html += '<div class="col box-image">';
    html += '<img src="' + src + '">';
    html += '</div>';
    html += '<div class="col title">';
    html += '<span>' + this.getAlt(alt) + '</span>';
    html += '</div>';
    html += '<div class="col link">';
    html += '<a href="' + src + '" download="' + (alt).toLowerCase() + '">';
    html += '<img src="assets/icon/download.png">';
    html += '</a>';
    html += '</div>';
    html += '</div>';

    return html;
};

Popup.prototype.getNoItem = function () {
    html = '<div class="item">';
    html += '<div class="col title full">';
    html += '<span>Nada por aqui...</span>';
    html += '</div>';
    html += '</div>';

    return html;
};

Popup.prototype.setTitle = function () {
    var self = this;

    chrome.tabs.getSelected(null, function (tab) {
        self.title = tab.title;
    });

    return this;
};

Popup.prototype.getAlt = function (alt) {
    if (!Boolean(alt)) {
        alt = this.title;
    }

    return alt;
};

new Popup();