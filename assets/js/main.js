function Main() {
    'use strict';

    this.init();
}

Main.prototype.init = function () {
    this.checkWebsite();
};

Main.prototype.checkWebsite = function () {
    var url = location.hostname;

    if (url.indexOf('lightinthebox.com') >= 0) {
        new LightInThebox();
    }
    else if (url.indexOf('shein.com') >= 0) {
        new SheIn();
    }
    else if(url.indexOf('choies.com') >= 0) {
        new Choies();
    }
    else if(url.indexOf('aliexpress.com') >= 0) {
        new AliExpress();
    }

    return this;
};

new Main();