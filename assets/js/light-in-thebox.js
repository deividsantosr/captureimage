function LightInThebox() {
    'use strict';

    this.constructor();
    this.init();
}

LightInThebox.prototype.constructor = function () {
    this.object = {
        items: []
    };
};

LightInThebox.prototype.init = function () {
    this.scanImage()
        .setStorage();
};

//Change this method on other occasions
LightInThebox.prototype.scanImage = function () {
    var self = this;

    jQuery('.viewport img[data-normal]').each(function () {
        var alt = jQuery(this).attr('alt');
        var src = jQuery(this).attr('data-normal');

        self.setObject(alt, src);
    });

    return this;
};

LightInThebox.prototype.setStorage = function () {
    chrome.storage.sync.set(this.object, function () {
        console.log('Saved configurations...');
    });

    return this;
};

LightInThebox.prototype.setObject = function (alt = 'Sem nome', src = '') {
    this.object.items.push([alt, src]);

    return this;
};