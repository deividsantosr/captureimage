function AliExpress() {
    'use strict';

    this.constructor();
    this.init();
}

AliExpress.prototype.constructor = function () {
    this.object = {
        items: []
    };
};

AliExpress.prototype.init = function () {
    this.scanImage()
        .setStorage();
};

//Change this method on other occasions
AliExpress.prototype.scanImage = function () {
    var self = this;
    var imageBigViewURL = null, listUrl = null;

    var toDelete = ['[', ']', new RegExp(/;/g), new RegExp(/"/g)];

    imageBigViewURL = document.querySelector('.detail-gallery').innerHTML.toString().replace(/\n/g, '');
    imageBigViewURL = imageBigViewURL.substr(imageBigViewURL.indexOf('window.runParams.imageBigViewURL'));
    imageBigViewURL = imageBigViewURL.substr(0, imageBigViewURL.indexOf('window.runParams.mainBigPic'));
    imageBigViewURL = imageBigViewURL.substr(imageBigViewURL.indexOf('['), imageBigViewURL.indexOf(']'));
    
    //Remove special char of the string
    for(x in toDelete) {
        imageBigViewURL = imageBigViewURL.replace(toDelete[x], '');
    }

    //Turn in array
    listUrl = imageBigViewURL.split(',');

    for(x in listUrl) {
        self.setObject('', listUrl[x]);
    }

    return this;
};

AliExpress.prototype.setStorage = function () {
    chrome.storage.sync.set(this.object, function () {
        console.log('Saved configurations...');
    });

    return this;
};

AliExpress.prototype.setObject = function (alt = 'Sem nome', src = '') {
    this.object.items.push([alt, src]);

    return this;
};